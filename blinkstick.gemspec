# -*- encoding: utf-8 -*-

require_relative 'lib/blinkstick/version'

Gem::Specification.new do |s|
    s.name        = 'blinkstick'
    s.version     = BlinkStick::VERSION
    s.summary     = "BlinkStick Ruby API"
    s.description =  <<~EOF
      
      API to communicated with BlinkStick device.

      EOF

    s.homepage    = 'https://gitlab.com/sdalu/blinkstick'
    s.license     = 'MIT'

    s.authors     = [ "Stéphane D'Alu" ]
    s.email       = [ 'sdalu@sdalu.com' ]

    s.extensions  = [ "ext/extconf.rb" ]
    s.files       = %w[ README.md blinkstick.gemspec ] 			+
		    Dir['ext/**/*.{c,h,rb}'] 				+
                    Dir['lib/**/*.rb']

    s.add_dependency 'libusb'
    s.add_development_dependency 'yard', '~>0'
    s.add_development_dependency 'rake', '~>13'
end
