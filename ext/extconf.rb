require 'mkmf'

unless find_header('libusb.h')
  abort "libusb header is missing"
end

unless find_library('usb', 'libusb_init')
  abort "libusb is missing"
end

create_makefile('blinkstick/impl/native')
