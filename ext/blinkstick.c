#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <libusb.h>
#include <time.h>
#include <ruby.h>

/*
 */

#define BLINKSTICK_MAX_LED 64

/* Hold the libusb context for this library
 */
static libusb_context *context = NULL;

			    
/* Blinkstick internal structure definition
 */
struct blinkstick {
    libusb_device_handle *devh;
    struct libusb_device_descriptor desc;
    struct timespec last;
    int throttling;
    int closed;
};

static void blinkstick_free(void *data) {
    struct blinkstick *bs = data;
    if (! bs->closed)
        libusb_close(bs->devh);
    free(data);
}
static size_t blinkstick_size(const void* data) {
    return sizeof(struct blinkstick);
}

static const rb_data_type_t blinkstick_type = {
    .wrap_struct_name = "blinkstick",
    .function         = { .dmark = NULL,
                          .dfree = blinkstick_free,
			  .dsize = blinkstick_size, },
    .data             = NULL,
    .flags            = RUBY_TYPED_FREE_IMMEDIATELY,
};


/*
 * Helper for throtling control_transfer
 */
static int
control_transfer(struct blinkstick *bs,
		 uint8_t bmRequestType, uint8_t bRequest,
		 uint16_t wValue, uint16_t wIndex,
		 unsigned char *data, uint16_t wLength,
		 unsigned int timeout,
		 unsigned int throttling) {
    struct timespec now;
    struct timespec delay;

    long usb_delay_ns = bs->throttling * 1000000;
    bs->throttling = throttling;
    
    clock_gettime(CLOCK_MONOTONIC, &now);
    timespecsub(&now, &bs->last, &delay);
    if ((delay.tv_sec == 0) && (delay.tv_nsec < usb_delay_ns)) {
        delay.tv_nsec = usb_delay_ns - delay.tv_nsec;
	while( clock_nanosleep(CLOCK_MONOTONIC, 0, &delay, &delay) == EINTR )
	    ;
    }
  
    int rc = libusb_control_transfer(bs->devh, bmRequestType, bRequest,
				     wValue, wIndex, data, wLength, timeout);

    clock_gettime(CLOCK_MONOTONIC, &bs->last);

    return rc;
}


/*
 *
 */

static VALUE cBlinkStick = Qundef;
static VALUE cIOError    = Qundef;
static ID    id_at_serial;
static ID    id_eq;
static ID    id_gt;
static ID    id_lt;
static ID    id_ge;
static ID    id_le;


static inline int color_from_grb(uint8_t *color_data) {
    return color_data[1] << 16 |
	   color_data[0] <<  8 |
	   color_data[2] <<  0 ;
};

static inline void color_to_grb(int color, uint8_t *color_data) {
    color_data[1] = color >> 16;
    color_data[0] = color >>  8;
    color_data[2] = color >>  0;
};

static inline void color_to_rgb(int color, uint8_t *color_data) {
    color_data[0] = color >> 16;
    color_data[1] = color >>  8;
    color_data[2] = color >>  0;
};

static inline int color_from_rgb(uint8_t *color_data) {
    return color_data[0] << 16 |
	   color_data[1] <<  8 |
	   color_data[2] <<  0 ;
};



static void _blinkstick_ensure_index_value(VALUE index) {
    Check_Type(index, T_FIXNUM);
    if (rb_funcall(index, id_ge, 1, INT2FIX(0)) == Qfalse ||
	rb_funcall(index, id_lt, 1, INT2FIX(BLINKSTICK_MAX_LED)) == Qfalse)
        rb_raise(rb_eArgError, "index need to be between 0 and 63");
}

static void _blinkstick_ensure_color_value(VALUE color) {
    Check_Type(color, T_FIXNUM);
    if (rb_funcall(color, id_ge, 1, INT2FIX(0x000000)) == Qfalse ||
	rb_funcall(color, id_le, 1, INT2FIX(0xffffff)) == Qfalse)
	rb_raise(rb_eArgError, "color need to be between #000000 and #FFFFFF");
}

static void _blinkstick_ensure_index(VALUE index, VALUE *from, VALUE *to) {
    int exclude_end;
    switch (TYPE(index)) {
    case T_NIL:
	*from = *to = Qnil;
        break;
    case T_FIXNUM:
        _blinkstick_ensure_index_value(index);
	*from = *to = Qnil;
	break;
    default:
        if (rb_range_values(index, from, to, &exclude_end)) {
	    if (exclude_end)
	        rb_raise(rb_eTypeError, "expected inclusive Range");
	    _blinkstick_ensure_index_value(*from);
	    _blinkstick_ensure_index_value(*to);

	} else {
	    rb_raise(rb_eTypeError, "expected Range or Fixnum type");
	}
	break;
    }
}


/* Get the list of blinkstick devices and return their serial number.
 * Note: Any I/O error while retrieving this list is ignored.
 */

static void _raise_blinkstick_ioerror_libusb(char *msg, int code) {
    rb_raise(cIOError, "%s (%s)", msg, libusb_strerror(code));
}

static void _blinkstick_ensure_opened(struct blinkstick *bs) {
    if (bs->closed) {
	rb_raise(cIOError, "device has been closed");
    }
}


static VALUE
_blinkstick_new(libusb_device_handle *devh,
		struct libusb_device_descriptor *desc,
		VALUE serial)
{   
    struct blinkstick *bs;
    VALUE obj = TypedData_Make_Struct(cBlinkStick, struct blinkstick,
				      &blinkstick_type, bs);
    bs->devh       = devh;
    bs->desc       = *desc;
    bs->throttling = 0;
    bs->closed     = 0;
    clock_gettime(CLOCK_MONOTONIC, &bs->last);
    
    rb_ivar_set(obj, id_at_serial, serial);

    return obj;
}

static VALUE
_blinkstick_lookup(VALUE id, VALUE list) {
  int idx = -1;
  
  /* Ensure well formed arguments (raise exception) 
     */
    if (! NIL_P(id  )) {
        if (RB_TYPE_P(id, T_FIXNUM)) { idx = RB_NUM2INT(id);    }
	else                         { id  = rb_str_to_str(id); }
    }
    if (! NIL_P(list)) Check_Type(list, T_ARRAY);

    /* Blinkstick instance 
     */
    VALUE bs = Qnil;

    /* Get device list 
     */
    libusb_device **devs;
    ssize_t cnt = libusb_get_device_list(context, &devs);
    if (cnt < 0) {
	rb_warning("libusb failed to get device list (%s)",
		   libusb_strerror(cnt));
	return bs;
    }
	
    /* Iterate over device list and fetch serial number
     * for identified blinkstick (vendor + product)
     */
    for (int i = 0 ; devs[i] ; i++) {
        libusb_device *dev = devs[i];
      
	/* Get descriptor */
	struct libusb_device_descriptor desc;
	int rc = libusb_get_device_descriptor(dev, &desc);
	if (rc < 0) {
	    rb_warning("libusb failed to get device descriptor (%s)",
		       libusb_strerror(rc));
	    continue;
	}

	/* Ensure it's a blinkstick */
	if ((desc.idVendor  != 0x20A0) || (desc.idProduct != 0x41E5))
	    continue;

	/* Get device handle */
	libusb_device_handle *devh = NULL;
	rc = libusb_open(dev, &devh);
	if (rc < 0) {
	    rb_warning("libusb failed to open device (%s)",
		       libusb_strerror(rc));
	    continue;
	}

	VALUE serial    = Qnil;
	char  data[256] = { 0 }; /* maximum descriptor size is 255 */
	int   size      = libusb_get_string_descriptor_ascii(
			     devh, desc.iSerialNumber, data, sizeof(data));
	if (size > 0) {
	    serial = rb_str_new(data, size);
	    RB_OBJ_FREEZE(serial);
	} else {
	    rb_warning("libusb failed to get string descriptor (%s)",
		       libusb_strerror(size));
	}

	if (!NIL_P(serial)) {
	  /* Push serial to list */
	  if (!NIL_P(list)) 
	      rb_ary_push(list, serial);

	  /* If requested idx/serial found, create BlinkStick instance
	   * with retrieved handle, don't close it, and break from loop
	   */
	  if (idx ==0) {
	      bs = _blinkstick_new(devh, &desc, serial);
	      break;
	  } else if (idx > 0) {
	      idx--;
	  } else if (!NIL_P(id) && RTEST(rb_str_equal(serial, id))) {
	      bs = _blinkstick_new(devh, &desc, serial);
	      break;
	  }
	}
	
	/* Close handle */
	libusb_close(devh);
    }
    libusb_free_device_list(devs, 1);
	
    /* Return blinkstick instance if found */
    return bs;
}

static VALUE blinkstick_serials(VALUE self) {
    VALUE list = rb_ary_new();
    _blinkstick_lookup(Qnil, list);
    return list;
}

static VALUE blinkstick_get(VALUE self, VALUE id) {
  if (NIL_P(id)) { id = INT2NUM(0); }
  return _blinkstick_lookup(id, Qnil);
}


static VALUE blinkstick__set_color(VALUE self, VALUE index, VALUE color) {
    VALUE from     = Qnil;
    VALUE to       = Qnil;
    _blinkstick_ensure_index(index, &from, &to);

    /* nil or value index */
    if (NIL_P(from)) {
	_blinkstick_ensure_color_value(color);
    /* range index with color array */
    } else if (RB_TYPE_P(color, T_ARRAY)) {
        int idx_from = RB_NUM2INT(from);
	int idx_to   = RB_NUM2INT(to  );
	int count    = abs(RB_NUM2INT(to) - RB_NUM2INT(from)) + 1;
	if (RARRAY_LEN(color) != count) {
	    rb_raise(rb_eArgError,
		     "expected a %d-element Array or an Integer color", count);
	}
	for (int i = 0 ; i < count ; i++) {
	    _blinkstick_ensure_color_value(RARRAY_AREF(color, i));
	}
	  
	if ((idx_from != 0) && (idx_to != 0)) {
	    rb_raise(rb_eArgError, "range must start/end at 0"
		                   " (due to firmware limitation)");
	}
	/* range index with single color */
    } else if (RB_TYPE_P(color, T_FIXNUM)) {
	_blinkstick_ensure_color_value(color);
    /* error */
    } else {
        rb_raise(rb_eTypeError, "expected Integer or Array");
    }
    
    struct blinkstick *bs;
    TypedData_Get_Struct(self, struct blinkstick, &blinkstick_type, bs);
    _blinkstick_ensure_opened(bs);
    
    int       throttling    = 1;
    uint8_t   bmRequestType = 0x20;
    uint8_t   bRequest      = 0x09;
    uint16_t  wValue        = 0x0000;
    uint16_t  wIndex        = 0x0000;
    uint8_t   data[2+BLINKSTICK_MAX_LED*3]  = { 0 };
    uint16_t  wLength;
    int       capacity;
    
    /* nil */
    if (NIL_P(index)) {
	wLength = 4;
	wValue  = 0x0001;
	color_to_rgb(RB_NUM2INT(color), &data[1]);
    /* value index */
    } else if (NIL_P(from)) {
	wLength = 6;
	wValue  = 0x0005;
	data[0] = 1;
	data[2] = RB_NUM2INT(index);
	color_to_rgb(RB_NUM2INT(color), &data[3]);
    /* range index (ex: 1..63) */
    } else {
	int idx_from = RB_NUM2INT(from);
	int idx_to   = RB_NUM2INT(to  );
	int idx_min  = idx_from < idx_to   ? idx_from : idx_to;
	int idx_max  = idx_to   > idx_from ? idx_to   : idx_from;
	int count    = 1 + idx_max - idx_min;
        if      (idx_max <  8) { capacity =  8; wValue = 0x0006; }
        else if (idx_max < 16) { capacity = 16; wValue = 0x0007; }
        else if (idx_max < 32) { capacity = 32; wValue = 0x0008; throttling=2; }
        else if (idx_max < 64) { capacity = 64; wValue = 0x0009; throttling=3; }
        else                   { rb_fatal("index >= 64");        }
	wLength = 2 + capacity * 3;

	/* Standard order */
	if (idx_from < idx_to) {
	    for (int i = 0 ; i < count ; i++) {
	        int c = RB_TYPE_P(color, T_ARRAY)
		      ? RARRAY_AREF(color, i)
		      : color;
		color_to_grb(RB_NUM2INT(c), &data[2 + i * 3]);
	    }	    
	/* Reversed order */
	} else {
	    for (int i = 0 ; i < count ; i++) {
	        int c =  RB_TYPE_P(color, T_ARRAY)
		      ? RARRAY_AREF(color, count - 1 - i)
		      : color;
	        color_to_grb(RB_NUM2INT(c), &data[2 + i * 3]);
	    }
	}
    }
    
    int rc = control_transfer(bs, bmRequestType, bRequest,
			      wValue, wIndex, data, wLength, 0, throttling);
    if (rc != wLength) {
	_raise_blinkstick_ioerror_libusb("control transfert", rc);
    }

    return color;
}




static VALUE blinkstick__get_color(VALUE self, VALUE index) {
    VALUE from     = Qnil;
    VALUE to       = Qnil;
    _blinkstick_ensure_index(index, &from, &to);

    struct blinkstick *bs;
    TypedData_Get_Struct(self, struct blinkstick, &blinkstick_type, bs);
    _blinkstick_ensure_opened(bs);

    int       throttling    = 1;
    uint8_t   bmRequestType = 0x80 | 0x20;
    uint8_t   bRequest      = 0x01;
    uint16_t  wValue;
    uint16_t  wIndex        = 0x0000;
    uint16_t  wLength;
    int       capacity;

    /* nil */
    if (NIL_P(index)) {
        wValue  = 0x0001;
        wLength = 4;
    /* range index or value index */
    } else {
	int idx_max  = 0;
	if (NIL_P(from)) {
	    idx_max = RB_NUM2INT(index);
	} else {
	    int idx_from = RB_NUM2INT(from);
	    int idx_to   = RB_NUM2INT(to  );
	    idx_max = idx_to > idx_from ? idx_to : idx_from;
	}
        if      (idx_max <  8) { capacity =  8; wValue = 0x0006; }
        else if (idx_max < 16) { capacity = 16; wValue = 0x0007; }
        else if (idx_max < 32) { capacity = 32; wValue = 0x0008; throttling=2; }
        else if (idx_max < 64) { capacity = 64; wValue = 0x0009; throttling=3; }
        else                   { rb_fatal("index >= 64");        }
        wLength  = 2 + capacity * 3;
    }

    /* usb transfert */
    uint8_t data[wLength];
    int     rc = control_transfer(bs, bmRequestType, bRequest,
				  wValue, wIndex, data, wLength, 0, throttling);
    if (rc != wLength) {
        _raise_blinkstick_ioerror_libusb("control transfert", rc);
    }

    /* nil */
    if (NIL_P(index)) {
        return INT2NUM(color_from_rgb(&data[1]));
    /* value index (ex: 1) */
    } else if (NIL_P(from)) {
	return INT2NUM(color_from_grb(&data[RB_NUM2INT(index) * 3 + 2]));
    /* range index (ex: 1..63) */
    } else {
        VALUE list   = rb_ary_new();
	int idx_from = RB_NUM2INT(from);
	int idx_to   = RB_NUM2INT(to  );

	/* Standard order */
	if (idx_from < idx_to) {
	    for (int i = idx_from ; i <= idx_to ; i++) {
		VALUE color = INT2NUM(color_from_grb(&data[i * 3 + 2]));
		rb_ary_push(list, color);
	    }
	/* Reversed order */
	} else {
	    for (int i = idx_from ; i >= idx_to ; i--) {
		VALUE color = INT2NUM(color_from_grb(&data[i * 3 + 2]));
		rb_ary_push(list, color);
	    }
	}
	return list;
    }
}


static void
no_instantiation(VALUE klass) {
    rb_undef_method(rb_singleton_class(klass), "new");
    rb_undef_alloc_func(klass);
}



static VALUE blinkstick__close(VALUE self)
{
    struct blinkstick *bs;
    TypedData_Get_Struct(self, struct blinkstick, &blinkstick_type, bs);

    if (!bs->closed) {
	libusb_close(bs->devh);
    }
    
    bs->closed = 1;
    return Qtrue;
}







void Init_native() {
    /* Initialise libusb */
    int r = libusb_init(&context);
    if (r < 0) {
	rb_raise(rb_eLoadError, "libusb initialization failure (%s)",
		 libusb_strerror(r));
    }

    /* Retrieve ruby references */
    id_at_serial = rb_intern("@serial");
    id_eq        = rb_intern("==");
    id_gt        = rb_intern(">");
    id_lt        = rb_intern("<");
    id_ge        = rb_intern(">=");
    id_le        = rb_intern("<=");

    /* Retrieve/Define classes */
    cBlinkStick = rb_define_class      (             "BlinkStick", rb_cObject);
    cIOError    = rb_define_class_under(cBlinkStick, "IOError",    rb_eIOError);

    /* Define BlinkStick class */
    no_instantiation(cBlinkStick);
    rb_define_singleton_method(cBlinkStick, "serials", blinkstick_serials, 0);
    rb_define_singleton_method(cBlinkStick, "[]",      blinkstick_get, 1);
    rb_define_attr(cBlinkStick, "serial", 1, 0);
    rb_define_method(cBlinkStick, "close",   blinkstick__close, 0);
    rb_define_method(cBlinkStick, "[]=",  blinkstick__set_color, 2);
    rb_define_method(cBlinkStick, "[]",   blinkstick__get_color, 1);
    
    /* Protect from garbage collection */
    rb_gc_register_address(&cBlinkStick);
    rb_gc_register_address(&cIOError);
}
