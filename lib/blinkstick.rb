class BlinkStick
    VENDOR_ID        = 0X20A0
    PRODUCT_ID       = 0x41E5
    COLOR_RANGE      = 0x000000 .. 0xFFFFFF
    INDEX_RANGE      = 0 .. 63
    MAX_LEDS         = 64
    BLOCKINFO_MAXLEN = 32

    class IOError < IOError
    end
    
    def color=(value)
        self[INDEX_RANGE] = value
    end

    def self.first
        self[nil]
    end
end


begin
  require 'blinkstick/impl/native'
rescue LoadError
  require 'blinkstick/impl/pure'
end
