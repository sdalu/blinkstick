require "libusb"

#
# NOTE: There is something fucked up in libusb as control_transfer returns
#       extra bytes (CONTROL_SETUP)
#

class BlinkStick
    @@usb  ||= LIBUSB::Context.new
    
    def self.serials
        @@usb.devices(:idVendor  => VENDOR_ID,
                      :idProduct => PRODUCT_ID)
             .map(&:serial_number)
    end
  
    def self.[](id)
        devlist = @@usb.devices(:idVendor  => VENDOR_ID,
                                :idProduct => PRODUCT_ID)
        device = case id
                 when String   then devlist.find {|dev| dev.serial_number==id }
                 when Integer  then devlist[id]
                 when NilClass then devlist.first
                 else raise ArgumentError, "expected String, Integer, or nil"
                 end
        if device
            BlinkStick.new(device)
        end
    end

     
    def initialize(device)
        @device     = device
        @serial     = device.serial_number.freeze
        @handle     = device.open
        @last       = Time.now
        @throttling = 0
    end

    def close
        @handle.close
    end
    
    def []=(index, color)
        if index.nil?
            set_first_color(color)
        elsif index.instance_of?(Integer) && color.instance_of?(Integer)
            set_index_color(index, color)
        else
            set_range_colors(index, color)
        end
    end

    def [](index)
        case index
        when nil     then get_first_color()
        when Integer then get_range_colors(index .. index).first
        when Range   then get_range_colors(index)
        end
    end

    
    def mode=(mode)
      control_transfer(:bmRequestType => 0x20,
                       :bRequest      => 0x09,
                       :wValue        => 0x0001,
                       :wIndex        => 0x0000,
                       :dataOut       => [4, mode].pack('c*'))
    end

    def info_block(id, data=nil)
        if data.nil?
            data = control_transfer(:bmRequestType => 0x80 | 0x20,
                                    :bRequest      => 0x01,
                                    :wValue        => id + 1,
                                    :wIndex        => 0x0000,
                                    :dataIn        => 33)
            data = data[1..-1]
            if eoc = data.index("\0")
            then data[0, eoc]
            else data
            end
        else
            remain = BLOCKINFO_MAXLEN - data.size
            data  += '\0' * remain if remain > 0
            data   = data[0, BLOCKINFO_MAXLEN]
            data   = (id + 1).chr + data
            
            control_transfer(:bmRequestType => 0x20,
                             :bRequest      => 0x09,
                             :wValue        => id + 1,
                             :wIndex        => 0x0000,
                             :dataOut       => data)
        end
    end

    
    private
    
    def ensure_color!(color)
        if ! COLOR_RANGE.include?(color)
            raise ArgumentError,
                  "color value expected in #%06x .. #%06x" % [
                      COLOR_RANGE.begin, COLOR_RANGE.end ]
        end
    end

    def ensure_index!(index)
        if ! INDEX_RANGE.include?(index)
            raise ArgumentError,
                  "index value expected in %d .. %d" % [
                      INDEX_RANGE.begin, INDEX_RANGE.end ]
        end
    end

    def control_transfer(throttling: 1, **opts)
        usb_delay, @throttling = @throttling, (throttling / 1000.0)
        delay = usb_delay - (Time.now - @last)
        sleep(delay) if delay > 0
        r = @handle.control_transfer(**opts)
        @last = Time.now
        r
    rescue LIBUSB::Error => e
        raise IOError, e.message
    end

    def get_first_color
        data = control_transfer(:bmRequestType => 0x80 | 0x20,
                                :bRequest      => 0x01,
                                :wValue        => 0x0001,
                                :wIndex        => 0x0000,
                                :dataIn        => 4)
        (data[1].ord << 16) | (data[2].ord << 8) | (data[3].ord << 0)
    end

    def set_first_color(color)
        ensure_color!(color)
        _, r, g, b = [ color ].pack('N').bytes

        data = [ 1, r, g, b ].pack('C*')
      
        control_transfer(:bmRequestType => 0x20,
                         :bRequest      => 0x9,
                         :wValue        => 0x0001,
                         :wIndex        => 0x0000,
                         :dataOut       => data)
    end

    def set_index_color(index, color)
        ensure_index!(index)
        ensure_color!(color)
        _, r, g, b = [ color ].pack('N').bytes

        data = [ 1, 0, index, r, g, b ].pack('C*')
      
        control_transfer(:bmRequestType => 0x20,
                         :bRequest      => 0x09,
                         :wValue        => 0x0005,
                         :wIndex        => 0x0000,
                         :dataOut       => data)
    end

    def get_range_colors(index)
        # Basic argument checking
        case index
        when Range
            ensure_index!(index.begin)
            ensure_index!(index.end)
        when Integer
            ensure_index!(index)
        else raise ArgumentError, "expected Range or Integer for index"
        end

        idx_max                      = [ index.begin, index.end ].max
        capacity, wValue, throttling = if    idx_max <=  8 then [  8, 6, 1 ]
                                       elsif idx_max <= 16 then [ 16, 7, 1 ]
                                       elsif idx_max <= 32 then [ 32, 8, 2 ]
                                       elsif idx_max <= 64 then [ 64, 9, 3 ]
                                       else raise "internal error"
                                       end

        wLength = 2 + capacity * 3
        data    = control_transfer(:bmRequestType => 0x80 | 0x20,
                                   :bRequest      => 0x01,
                                   :wValue        => wValue,
                                   :wIndex        => 0x0000,
                                   :dataIn        => wLength,
                                   :throttling    => throttling)
        colors = data[2..(wLength - 1)].bytes.each_slice(3).map{ |g, r, b|
            (r << 16) | (g << 8) | (b << 0)
        }

        if  index.begin < index.end
        then colors[index]
        else colors[(index.end)..(index.begin)].reverse
        end
    end

    
    def set_range_colors(index, colors)
        # Basic argument checking
        case index
        when Range
            ensure_index!(index.begin)
            ensure_index!(index.end)
        when Integer
            ensure_index!(index)
        else raise ArgumentError, "expected Range or Integer for index"
        end

        case colors
        when Array
            if colors.empty?
                raise ArgumentError, "expected non-empty Array for color"
            end
            colors.each {|c| ensure_color!(c) }
        when Integer
            ensure_color!(colors)
        else raise ArgumentError, "expected Array or Integer for color"
        end

        # Normalize arguments
        case index
        when Range
            if index.begin > index.end
                index  = index.end .. index.begin
                colors = colors.reverse if Array === colors
            end
            count = index.end - index.begin + 1
            case colors
            when Array
                if count != colors.size
                    raise ArgumentError,
                          "expected an %d-element Array or an Integer color" % count
                end
            when Integer
                colors = [ colors ] * count
            end
        when Integer
            case colors
            when Array
                last  = [ index + colors.size - 1, MAX_LEDS - 1 ].min
                index = index .. last
            when Integer
                index = index .. index
            end
        end

        # Firmware limitation
        if (index.begin != 0) & (index.end != 0)
            raise ArgumentError,
                  "index must start/end at 0 (due to firmware limitation)"
        end
        
        # Build data
        count                        = colors.size
        capacity, wValue, throttling = if    count <=  8 then [  8, 6, 1 ]
                                       elsif count <= 16 then [ 16, 7, 1 ]
                                       elsif count <= 32 then [ 32, 8, 2 ]
                                       elsif count <= 64 then [ 64, 9, 3 ]
                                       else raise "internal error"
                                       end
        colors += [ 0 ] * (capacity - count)
        data = ([ capacity, 0 ] + colors.flat_map {|color|
                  _, r, g, b = [ color ].pack('N').bytes
                  [ g, r, b ]
                }).pack('C*')

        control_transfer(:bmRequestType => 0x20,
                         :bRequest      => 0x09,
                         :wValue        => wValue,
                         :wIndex        => 0x0000,
                         :dataOut       => data,
                         :throttling    => throttling)
    end
end
