# Get BlinkStick instance (USB device)
$bs = BlinkStick.first             # First available
$bs = BlinkStick[1]                # Second BlinkStick (index start at 0)
$bs = BlinkStick['BS040270-3.0']   # BlinkStick identified by serial number

# Set color (using 3 differents firmware API)
$bs[nil]  = 0xff0000               #1# Default led (ie: first led)
$bs[1]    = 0xff0000               #2# Led at index 1
$bs[0..3] = 0xff0000               #3# The first four led

# Get color (using 2 differents firmware API)
$bs[nil]                           #1# Default led (ie: first led)
$bs[1]                             #2# Led at index 1
$bs[0..3]                          #3# The first four led

# Release resource
$bs.close
